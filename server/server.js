const express = require('express');
const path = require('path');
const http = require('http');
const socketIO = require('socket.io');
const {generatMessage, generatLocationMessage} = require('./utils/message');
const { isRealString } = require('./utils/validation');
const { User } = require('./utils/user');

var app = express();
var publicPath = path.join(__dirname, '../public');
var port = process.env.PORT || 3000;
var server = http.createServer(app);
var io = socketIO(server);
let userObj = new User();

app.use(express.static(publicPath));

io.on('connection', (socket) => {

    socket.on('join', (params, callback) => {
        if (!isRealString(params.name) || !isRealString(params.room)) {
            return callback("Name and room are required..");
        }
        
        socket.join(params.room);

        userObj.removeUser(socket.id);
        userObj.addUser(socket.id, params.name, params.room);
        
        io.to(params.room).emit('updateUsersList', {
            user: userObj.getUserList(params.room),
            room: params.room
        });
    
        socket.broadcast.to(params.room).emit('new-message', generatMessage("Admin", `${params.name} has joined`));
        callback();
    });

    socket.emit('new-message', generatMessage("Admin", "Welcome to the chat"));

    socket.on('create-message', (message, callback) => {
        let user = userObj.getUserById(socket.id);
        if (user && isRealString(message.text)) {
            io.to(user.room).emit('new-message', generatMessage(user.name, message.text));
            callback('from the server..');
        }
    });

    socket.on('current-location', (message, callback) => {
        let user = userObj.getUserById(socket.id);
        if(user) {
            io.to(user.room).emit('new-location-message', generatLocationMessage(user.name, message.latitude, message.longitude));
            callback();
        }
    });

    socket.on('disconnect', () => {
        var user = userObj.removeUser(socket.id);
        if(user) {
            io.to(user.room).emit('updateUsersList', {
                user: userObj.getUserList(user.room),
                room: user.room
            });
            io.to(user.room).emit('new-message', generatMessage("Admin", `${user.name} has left`));
        }
    });
});


server.listen(port, () => {
    console.log(`Server are running on port ${port}`);
});