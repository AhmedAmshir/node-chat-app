const expect = require('expect');

var {generatMessage, generatLocationMessage} = require('./message');

describe('generateMessage', () => {
    it('Should generate message object', () => {
        var from = "Ahmed";
        var text = "Test message";
        var message = generatMessage(from, text);

        expect(typeof message.createdAt).toBe('string');
        expect(message).toMatchObject({
            from,
            text
        });
    });
});

describe('generateLocationMessage', () => {
    it('Should generate user location', () => {
        var from = "Ahmed";
        var latitude = 15;
        var longitude = 20;
        var url = "https://www.google.com/maps?q=15,20";

        var message = generatLocationMessage(from, latitude, longitude);

        expect(typeof message.createdAt).toBe('string');
        expect(message).toMatchObject({
            from,
            latitude,
            longitude
        });
    });
});