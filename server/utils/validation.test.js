const expect = require('expect');

const {isRealString} = require('./validation');

describe("Describe isRealString", () => {
    it('Should reject non-real string', () => {
        var res = isRealString(55);
        expect(res).toBe(false);
    });
    it('Should reject string with only spaces', () => {
        var res = isRealString('   ');
        expect(res).toBe(false);
    });
    it('Should allow string with non-space characters', () => {
        var res = isRealString(" test ");
        expect(res).toBe(true);
    });
});