const expect = require('expect');
const {User} = require('./user');



describe('User class', () => {

    let users;

    beforeEach(() => {
        users = new User();
        users.users = [{
            id: 1,
            name: "ahmed",
            room: "developer"

        },
        {
            id: 2,
            name: "ali",
            room: "tester"

        },
        {
            id: 3,
            name: "moustafa",
            room: "developer"

        }]
    });

    it('Should add new user', () => {
        let users = new User();
        let user = {
            id: 4,
            name: "user1",
            room: "room1"
        };
        let resUser = users.addUser(user.id, user.name, user.room);
        expect(users.users).toEqual([resUser]);
    });

    it('Should remove only one user', () => {
        let userId = 1;
        let user = users.removeUser(userId);
        
        expect(user.id).toBe(userId);
        expect(users.users.length).toBe(2);
    });

    it('Should not remove user', () => {
        let userId = 99;
        let user = users.removeUser(userId);

        expect(user).toBeFalsy();
        expect(users.users.length).toBe(3);
    });

    it("should find user", () => {
        let userId = 2;
        let user = users.getUserById(userId);
        expect(user.id).toBe(userId);
    });

    it("should not find user", () => {
        let userId = 10;
        let user = users.getUserById(userId);
        expect(user).toBeFalsy();
    });

    it('Should return names of users in developer room', () => {
        let usersList = users.getUserList("developer");
        expect(usersList).toEqual(["ahmed", "moustafa"]);
    });

    it('Should return names of users in tester room', () => {
        let usersList = users.getUserList("tester");
        expect(usersList).toEqual(["ali"]);
    });
});