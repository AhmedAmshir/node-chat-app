var moment = require('moment');

var dateFormat = moment().format("hh:mm a");

var generatMessage = (from, text) => {
    return {
        from,
        text,
        createdAt: dateFormat
    }
}

var generatLocationMessage = (from, latitude, longitude) => {
    return {
        from,
        latitude,
        longitude,
        url: `https://www.google.com/maps?q=${latitude},${longitude}`,
        createdAt: dateFormat
    }
}

module.exports = {
    generatMessage,
    generatLocationMessage
};