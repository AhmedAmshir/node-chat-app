var socket = io();
socket.on('connect', () => {
    var params = $.deparam(window.location.params);

    socket.emit('join', params, function (err) {
        if(err) {
            window.location.href('/');
        } else {
            console.log("No error..");
        }
    });
});

socket.on('disconnect', () => {
    console.log('Disconnected from server..');
});

socket.on('updateUsersList', (userData) => {
    let ol = document.createElement("ol");
    let users = userData.user;
    
    users.forEach((user) => {
        let li = document.createElement("li");
        li.textContent = user;
        ol.append(li);
    });

    $('#room').html(userData.room);
    $('#users').html(ol);
});

socket.on('welcome-message', (message) => {
    console.log(message);
});

socket.on('new-message', (message) => {

    var template = $('#message-template').html();
    var html = Mustache.render(template, {
        from: message.from,
        text: message.text,
        createdAt: message.createdAt
    });

    $('#messages').append(html);
    messageModalscrollToDown();
    // $('#messages').append(`<li>${message.from} - <i>${message.createdAt}</i> : ${message.text}</li>`);
});

socket.on('new-location-message', (message) => {


    var template = $('#message-template').html();
    var html = Mustache.render(template, {
        from: message.from,
        text: 'My location',
        url: message.url,
        createdAt: message.createdAt
    });

    $('#messages').append(html);
    messageModalscrollToDown();

    // var a = document.createElement("a");
    // var li = document.createElement("li");
    // li.textContent = `${message.from} - <i>${message.createdAt}</i> : `;
    // a.textContent = 'My location';
    // a.setAttribute("href", message.url);
    // a.setAttribute("target", '_blank');
    // li.append(a);
    // $('#messages').append(li);
});

$(document).ready(function() {
    $('#chat-form').on('submit', function(e) {
        e.preventDefault();

        if($('#message').val() != '') {
            socket.emit('create-message', {
                text: $('#message').val()
            }, (data) => {
                $('#message').val('');
            });
        }
    });

    $('#my-location').on('click', (e) => {
        e.preventDefault();
        if(!navigator.geolocation) {
            alert("THis browser doesn't be supported with geolocation..");
        }

        $('#my-location').attr('disabled', 'disabled');
        navigator.geolocation.getCurrentPosition((position) => {
            socket.emit('current-location', {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
            }, function() {
                $('#my-location').removeAttr('disabled');
            });
        });
    });
});

function messageModalscrollToDown() {
    var objDiv = document.getElementById('messages');
    objDiv.scrollTop = objDiv.scrollHeight;
}